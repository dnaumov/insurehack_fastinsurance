#import "_User.h"

@interface User : _User {}

- (void)importWithDictionary:(NSDictionary *)dict;

@end
