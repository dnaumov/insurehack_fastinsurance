//
//  DataLoader.m
//  SimpelSicherung
//
//  Created by Dima on 18/11/2016.
//  Copyright © 2016 Dmytro Naumov. All rights reserved.
//

#import "DataLoader.h"

static const int kMaxLoadRequests = 3;

@interface DataLoader()
{
    NSOperationQueue *loadQueue;
}

@end

@implementation DataLoader

+ (instancetype) sharedLoader
{
    static id sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        [sharedInstance setup];
    });
    return  sharedInstance;
}

- (void) setup
{
    loadQueue = [NSOperationQueue new];
    [loadQueue setMaxConcurrentOperationCount:kMaxLoadRequests];
}

- (void)loadDataFromUrl:(NSString *)urlStr WithCompetionHandler:(void (^)(NSArray* _Nullable result)) handler
{
    NSURL *url = [NSURL URLWithString:urlStr];
    NSLog(@"Load URL: %@", urlStr);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    __block void (^completionHandler)(NSArray* _Nullable result) = handler;
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:loadQueue
                           completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
                               NSArray *result = nil;
                               NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
                               if (urlResponse.statusCode != 200 || data == nil){
                                   NSLog(@"Data load error: %@", urlResponse.description);
                               }
                               else { //try to parse data as json
                                   NSError *jsonParseError = nil;
                                   NSArray *parsedArr = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonParseError];
                                   if (jsonParseError == nil && [parsedArr isKindOfClass:[NSArray class]]){
                                       result = parsedArr;
                                   }
                                   else {
                                       NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                       NSLog(@"JSON parse error %@ \n%@", jsonParseError.description, jsonString);
                                   }
                               }
                               if (completionHandler != nil){
                                   completionHandler(result);
                               }
                           }];
}

@end
