//
//  CoreDataImport.h
//  SimpelSicherung
//
//  Created by Dima on 19/11/2016.
//  Copyright © 2016 Dmytro Naumov. All rights reserved.
//

#ifndef CoreDataImport_h
#define CoreDataImport_h

#import <MagicalRecord/MagicalRecord.h>
#import "User.h"
#import "InsuranceItem.h"

#endif /* CoreDataImport_h */
