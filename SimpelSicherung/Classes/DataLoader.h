//
//  DataLoader.h
//  SimpelSicherung
//
//  Created by Dima on 18/11/2016.
//  Copyright © 2016 Dmytro Naumov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataLoader : NSObject

+ (instancetype) sharedLoader;
- (void)loadDataFromUrl:(NSString *)urlStr WithCompetionHandler:(void (^)(NSArray* _Nullable result)) handler;
@end
