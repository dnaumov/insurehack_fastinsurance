// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to InsuranceItem.m instead.

#import "_InsuranceItem.h"

const struct InsuranceItemAttributes InsuranceItemAttributes = {
	.isOrdered = @"isOrdered",
	.itemDescription = @"itemDescription",
	.itemGroup = @"itemGroup",
	.itemId = @"itemId",
	.itemName = @"itemName",
	.preisPerQuantity = @"preisPerQuantity",
	.quantityOrdered = @"quantityOrdered",
	.quantityTypeStr = @"quantityTypeStr",
};

@implementation InsuranceItemID
@end

@implementation _InsuranceItem

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"InsuranceItem" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"InsuranceItem";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"InsuranceItem" inManagedObjectContext:moc_];
}

- (InsuranceItemID*)objectID {
	return (InsuranceItemID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic isOrdered;

@dynamic itemDescription;

@dynamic itemGroup;

@dynamic itemId;

@dynamic itemName;

@dynamic preisPerQuantity;

@dynamic quantityOrdered;

@dynamic quantityTypeStr;

@end

