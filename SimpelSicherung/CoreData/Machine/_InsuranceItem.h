// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to InsuranceItem.h instead.

#import <CoreData/CoreData.h>

extern const struct InsuranceItemAttributes {
	__unsafe_unretained NSString *isOrdered;
	__unsafe_unretained NSString *itemDescription;
	__unsafe_unretained NSString *itemGroup;
	__unsafe_unretained NSString *itemId;
	__unsafe_unretained NSString *itemName;
	__unsafe_unretained NSString *preisPerQuantity;
	__unsafe_unretained NSString *quantityOrdered;
	__unsafe_unretained NSString *quantityTypeStr;
} InsuranceItemAttributes;

@interface InsuranceItemID : NSManagedObjectID {}
@end

@interface _InsuranceItem : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) InsuranceItemID* objectID;

@property (nonatomic, strong) NSString* isOrdered;

//- (BOOL)validateIsOrdered:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* itemDescription;

//- (BOOL)validateItemDescription:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* itemGroup;

//- (BOOL)validateItemGroup:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* itemId;

//- (BOOL)validateItemId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* itemName;

//- (BOOL)validateItemName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* preisPerQuantity;

//- (BOOL)validatePreisPerQuantity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* quantityOrdered;

//- (BOOL)validateQuantityOrdered:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* quantityTypeStr;

//- (BOOL)validateQuantityTypeStr:(id*)value_ error:(NSError**)error_;

@end

@interface _InsuranceItem (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveIsOrdered;
- (void)setPrimitiveIsOrdered:(NSString*)value;

- (NSString*)primitiveItemDescription;
- (void)setPrimitiveItemDescription:(NSString*)value;

- (NSString*)primitiveItemGroup;
- (void)setPrimitiveItemGroup:(NSString*)value;

- (NSString*)primitiveItemId;
- (void)setPrimitiveItemId:(NSString*)value;

- (NSString*)primitiveItemName;
- (void)setPrimitiveItemName:(NSString*)value;

- (NSString*)primitivePreisPerQuantity;
- (void)setPrimitivePreisPerQuantity:(NSString*)value;

- (NSString*)primitiveQuantityOrdered;
- (void)setPrimitiveQuantityOrdered:(NSString*)value;

- (NSString*)primitiveQuantityTypeStr;
- (void)setPrimitiveQuantityTypeStr:(NSString*)value;

@end
